package corona.CRUDprojeto.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "afilhado")
@JsonIgnoreProperties({"hibernateLazyInitializer","handler"})
public class Afilhado {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long afilhado_id;
    private String firstName;
    private String lastName;

    @ManyToOne
    @JoinColumn(name = "padrinho_id")
    @JsonBackReference
    private Padrinho padrinho;

    @ManyToMany(fetch = FetchType.LAZY,
                cascade = {CascadeType.PERSIST, CascadeType.MERGE,
                CascadeType.DETACH, CascadeType.REFRESH})
    @JoinTable(
            name = "afilhado_curso",
            joinColumns = @JoinColumn(name = "afilhado_id"),
            inverseJoinColumns = @JoinColumn(name = "curso_id"))
    //@JsonManagedReference
    private List<Curso> cursos = new ArrayList<>();

    public Afilhado(){
    }

    public Afilhado(String firstName, String lastName){
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public Padrinho getPadrinho() {
        return padrinho;
    }

    public void setPadrinho(Padrinho padrinho) {
        this.padrinho = padrinho;
    }

    public List<Curso> getCursos() {
        return cursos;
    }

    public void setCursos(List<Curso> cursos) {
        this.cursos = cursos;
    }

    public Long getAfilhado_id() {
        return afilhado_id;
    }

    public void setAfilhado_id(Long afilhado_id) {
        this.afilhado_id = afilhado_id;
    }

    @Column(name = "first_name", nullable = false)
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @Column(name = "last_name", nullable = false)
    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}
