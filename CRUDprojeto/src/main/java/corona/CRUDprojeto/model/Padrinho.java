package corona.CRUDprojeto.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import org.springframework.stereotype.Component;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Component
@Table(name = "padrinho")
@JsonIgnoreProperties({"hibernateLazyInitializer","handler"})
public class Padrinho {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long padrinho_id;
    private String firstName;
    private String lastName;

    @OneToMany(mappedBy = "padrinho", cascade = CascadeType.PERSIST)
    @JsonManagedReference
    private List<Afilhado> afilhados = new ArrayList<>();

    public Padrinho(){
    }

    public Padrinho(String firstName, String lastName){
        this.firstName = firstName;
        this.lastName = lastName;
    }


    public Long getPadrinho_id() {
        return padrinho_id;
    }

    public void setPadrinho_id(Long padrinho_id) {
        this.padrinho_id = padrinho_id;
    }

    @Column(name = "first_name", nullable = false)
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @Column(name = "last_name", nullable = false)
    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public List<Afilhado> getAfilhados() {
        return afilhados;
    }

    public void setAfilhados(List<Afilhado> afilhados) {
        this.afilhados = afilhados;
    }

    public void addAfilhado(Afilhado afilhado) {
        afilhados.add(afilhado);
        afilhado.setPadrinho(this);
    }
}
