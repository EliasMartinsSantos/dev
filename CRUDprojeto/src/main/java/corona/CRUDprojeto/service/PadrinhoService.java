package corona.CRUDprojeto.service;

import corona.CRUDprojeto.model.Padrinho;
import corona.CRUDprojeto.repository.PadrinhoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PadrinhoService {

    @Autowired
    private PadrinhoRepository padrinhoRepository;

    public PadrinhoService(){
    }

    public List<Padrinho> findAll(){
        return padrinhoRepository.findAll();
    }

    public List<Padrinho> findByName(String name){
        return padrinhoRepository.findByFirstName(name);
    }

    public Padrinho findById(Long id){
        return padrinhoRepository.getOne(id);
    }

    public Padrinho saveAndFlush(Padrinho padrinho){
        return padrinhoRepository.saveAndFlush(padrinho);
    }

    public void deleteById(Long id){
        padrinhoRepository.deleteById(id);
    }

    public void deleteByName(String name){
        padrinhoRepository.deleteByFirstName(name);
    }
}
