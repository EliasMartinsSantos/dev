package corona.CRUDprojeto.service;

import corona.CRUDprojeto.model.Curso;
import corona.CRUDprojeto.repository.CursoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CursoService {

    @Autowired
    private CursoRepository cursoRepository;

    public List<Curso> findAll() {
        return cursoRepository.findAll();
    }

    public Curso findById(Long id) {
        return cursoRepository.getOne(id);
    }

    public List<Curso> findByName(String name) {
        return cursoRepository.findByName(name);
    }

    public Curso saveAndFlush(Curso curso) {
        return cursoRepository.saveAndFlush(curso);
    }

    public void deleteById(Long id) {
        cursoRepository.deleteById(id);
    }

    public void deleteByName(String name) {
        cursoRepository.deleteByName(name);
    }
}
