package corona.CRUDprojeto.service;

import corona.CRUDprojeto.model.Afilhado;
import corona.CRUDprojeto.repository.AfilhadoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AfilhadoService {

    @Autowired
    private AfilhadoRepository afilhadoRepository;

    public AfilhadoService(){
    }

    public List<Afilhado> findAll() {
        return afilhadoRepository.findAll();
    }

    public List<Afilhado> findByName(String name){
        return afilhadoRepository.findByFirstName(name);
    }

    public Afilhado findById(Long id) {
        return afilhadoRepository.getOne(id);
    }

    public Afilhado saveAndFlush(Afilhado afilhado) {
        return afilhadoRepository.saveAndFlush(afilhado);
    }

    public void deleteById(Long id) {
        afilhadoRepository.deleteById(id);
    }

    public void deleteByName(String name) {
        afilhadoRepository.deleteByFirstName(name);
    }

}
