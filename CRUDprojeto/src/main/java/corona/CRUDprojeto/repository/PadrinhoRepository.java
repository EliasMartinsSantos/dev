package corona.CRUDprojeto.repository;

import corona.CRUDprojeto.model.Padrinho;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PadrinhoRepository extends JpaRepository<Padrinho, Long> {

    List<Padrinho> findByFirstName(String firstName);

    void deleteByFirstName(String firstName);
}
