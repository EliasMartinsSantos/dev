package corona.CRUDprojeto.repository;

import corona.CRUDprojeto.model.Afilhado;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AfilhadoRepository extends JpaRepository<Afilhado,Long> {

    List<Afilhado> findByFirstName(String firstName);

    void deleteByFirstName(String firstName);
}
