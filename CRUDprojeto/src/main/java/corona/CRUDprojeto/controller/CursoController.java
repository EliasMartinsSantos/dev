package corona.CRUDprojeto.controller;

import corona.CRUDprojeto.model.Curso;
import corona.CRUDprojeto.service.CursoService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/curso")
public class CursoController {

    @Autowired
    private CursoService cursoService;

    @GetMapping
    public List <Curso> getAllCurso(){return cursoService.findAll();}

    @GetMapping
    @RequestMapping("{id}")
    public Curso get(@PathVariable Long id){return cursoService.findById(id);}

    @GetMapping
    @RequestMapping("{name}")
    public List<Curso> getByName(@PathVariable String name){return cursoService.findByName(name);}

    @PostMapping
    public Curso create(@RequestBody final Curso curso){ return cursoService.saveAndFlush(curso);}

    @RequestMapping(value = "{id}", method = RequestMethod.DELETE)
    public void delete(@PathVariable Long id){ cursoService.deleteById(id);}

    @RequestMapping(value = "{name}", method = RequestMethod.DELETE)
    public void deleteByName(@PathVariable String name){ cursoService.deleteByName(name);}

    @RequestMapping(value = "{id}", method = RequestMethod.PUT)
    public Curso update(@PathVariable Long id, @RequestBody Curso curso){
        Curso existingCurso = cursoService.findById(id);
        BeanUtils.copyProperties(curso, existingCurso, "curso_id");
        return cursoService.saveAndFlush(existingCurso);
    }
}
