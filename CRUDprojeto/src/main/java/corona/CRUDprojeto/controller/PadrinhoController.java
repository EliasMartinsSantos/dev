package corona.CRUDprojeto.controller;

import corona.CRUDprojeto.model.Padrinho;
import corona.CRUDprojeto.service.PadrinhoService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/padrinho")
public class PadrinhoController {

    @Autowired
    private PadrinhoService padrinhoService;

    @GetMapping
    public List <Padrinho> getAllPadrinho(){
        return padrinhoService.findAll();
    }

    @GetMapping("{name}")
    public List <Padrinho> getByName(@PathVariable String name){
        return padrinhoService.findByName(name);
    }

    @GetMapping
    @RequestMapping("{id}")
    public Padrinho get(@PathVariable Long id){
        return padrinhoService.findById(id);
    }

    @PostMapping
    public Padrinho create(@RequestBody final Padrinho padrinho){
        return padrinhoService.saveAndFlush(padrinho);
    }

    @RequestMapping(value = "{id}", method = RequestMethod.DELETE)
    public void delete(@PathVariable Long id){
        padrinhoService.deleteById(id);
    }

    @RequestMapping(value = "{name}", method = RequestMethod.DELETE)
    public void deleteByName(@PathVariable String name){
        padrinhoService.deleteByName(name);
    }

    @RequestMapping(value = "{id}", method = RequestMethod.PUT)
    public Padrinho update(@PathVariable Long id, @RequestBody Padrinho padrinho){
        Padrinho existingPadrinho = padrinhoService.findById(id);
        BeanUtils.copyProperties(padrinho, existingPadrinho, "padrinho_id");
        return padrinhoService.saveAndFlush(existingPadrinho);
    }
}
